const express = require('express');
const router = express.Router();
//const querystring = require('querystring');
const request = require('request');

const DropboxController = require('../../controllers/DropboxController');
const GDriveController = require('../../controllers/GDriveController');
const CloudController = require('../../controllers/CloudController');


/** delete a folder PARAMS = path */
router.delete('/',function(req,res){
  let targets = JSON.parse(req.headers['target-to-delete']);
  let dropbox_path = targets['dropbox'];
  let google_id = targets['google'];

  let nbAsyncFinished = 0;

  if(dropbox_path != undefined){
    let dropbox = new DropboxController();
    dropbox.delete(dropbox_path,req,res,(body) => {
      console.log('delete folder Dropbox resp ...');
      nbAsyncFinished++;
      if(google_id == undefined || nbAsyncFinished == 2){
        success(res);
      }
    });
  }

  if(google_id != undefined){
    let google = new GDriveController();
    google.delete(google_id,req,res,(body) => {
      console.log('delete folder Google resp ...');
      nbAsyncFinished++;
      if(dropbox_path == undefined || nbAsyncFinished == 2){
        success(res);
      }
    });
  }

});

function success(res){
  res.json({status : 200, message : "successful deletion"});
}


module.exports = router;
