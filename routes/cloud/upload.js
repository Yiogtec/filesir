const express = require('express');
const router = express.Router();
//const querystring = require('querystring');
const request = require('request');
var Busboy = require('busboy');

const DropboxController = require('../../controllers/DropboxController');
const GDriveController = require('../../controllers/GDriveController');
const CloudController = require('../../controllers/CloudController');

/** upload a file PARAMS = path*/
router.post('/', function(req,res){

  var busboy = new Busboy({ headers: req.headers });
  var fileData = null;
  var body =  {};
  var mimetype_file = null;

  busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
    file.on('data', function(data) {
      fileData = data;
      mimetype_file = mimetype;
    });
    file.on('end', function() {
    });
  });

  busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated) {
    body[fieldname] = val;
  });

  busboy.on('finish', function() {
    console.log('Done parsing form!');
    console.log(body);
    // res.writeHead(303, { Connection: 'close', Location: '/' });

    let dropbox_path = body.dropbox;
    dropbox_path = dropbox_path + '/' + body.name;

    if (body.dropbox != 'undefined' && body.dropbox) {
      let dropbox = new DropboxController();
      dropbox.upload(dropbox_path, fileData, req, res, (body) => {
        res.json(body);
      });
    } else if(body.google) {
      let google = new GDriveController();
      google.upload(body.google, body.name, mimetype_file, fileData, req, res, (body) => {
        res.json(body);
      });
    }
  });

  req.pipe(busboy);
});



module.exports = router;
