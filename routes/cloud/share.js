const express = require('express');
const router = express.Router();
const URL = require('url');
const request = require('request');

const DropboxController = require('../../controllers/DropboxController');
const GDriveController = require('../../controllers/GDriveController');
const CloudController = require('../../controllers/CloudController');

var merge = function(res, body_path_lower, body_name) {

  let json = {"path_lower": body_path_lower, "name" : body_name}
  res.json(json);
  res.end();
}

router.patch('/',function(req,res){

  var nbAsyncFinished = 0;

  let google_id = req.body.google;
  let google_email = req.body.googleUserEmail;

  let dropbox_id = req.body.dropboxId;
  let dropbox_email = req.body.dropboxUserEmail;

  let body_path_lower = '';
  let body_name = '';

  if(dropbox_id && dropbox_email){
    nbAsyncFinished++;
    let dropbox = new DropboxController();
    console.log(dropbox_id);
    dropbox.share(dropbox_id, dropbox_email, req, res, (body) => {
      console.log('share dropbox  resp ...');
      if (google_id === undefined || nbAsyncFinished === 2) {
        body_path_lower = body.path_lower;
        merge(res, body_path_lower, body_name); // defined before the route
      }
    });
  }

  if (google_id && google_email) {
    nbAsyncFinished++;
    let google = new GDriveController();
    google.share(google_id, google_email, req, res, (body) => {
      console.log('share google  resp ...');
      if (dropbox_id === undefined || nbAsyncFinished === 2) {
        body_name = body.title;
        merge(res, body_path_lower, body_name);
      }
    });
  }
});


module.exports = router;
