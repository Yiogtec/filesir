const express = require('express');
const router = express.Router();
//const querystring = require('querystring');
const request = require('request');
const events = require('events');

const DropboxController = require('../../controllers/DropboxController');
const GDriveController = require('../../controllers/GDriveController');
const CloudController = require('../../controllers/CloudController');

var merge = function(res, body_path_lower, body_name) {

  let json = {"path_lower": body_path_lower, "name" : body_name}
  res.json(json);
  res.end();
}

/** move file PARAMS = fromPath toPath */
router.put('/',function(req,res){

  var nbAsyncFinished = 0;

  let dropbox_path = req.body.dropbox;
  let name = req.body.name;
  let google_path = req.body.google;

  let body_path_lower = '';
  let body_name = name;

  if(dropbox_path != undefined){
    var n = dropbox_path.lastIndexOf('/');
    var resultat = dropbox_path.substring(0,n + 1);
    let name_dropbox = resultat + name;

    let dropbox = new DropboxController();

    dropbox.move(dropbox_path, name_dropbox, req, res,(body) => {
      nbAsyncFinished++;
      console.log('rename folder resp ...');
      dropbox_body = body;
      if (google_path === undefined || nbAsyncFinished === 2) {
        body_path_lower = body.path_lower;
        merge(res, body_path_lower, body_name); // defined before the route
      }
    });

  }

  if(google_path){
    let google = new GDriveController();
    nbAsyncFinished++;
    console.log(google_path);
    console.log(name);
    google.edit(google_path, name, req, res, (body) =>{
      console.log('rename folder resp gdrive ...');
      if (dropbox_path === undefined || nbAsyncFinished === 2) {
        body_name = body.title;
        merge(res, body_path_lower, body_name);
      }

    });

  }
});

module.exports = router;
