const express = require('express');
const router = express.Router();
//const querystring = require('querystring');
const request = require('request');
const events = require('events');

const eventEmitter = new events.EventEmitter();
const DropboxController = require('../../controllers/DropboxController');
const GDriveController = require('../../controllers/GDriveController');

let nbAsyncFinished = 0;

eventEmitter.on('space',spaceCallback);

router.get('/',function(req,res){
  let dropbox = new DropboxController();
  let drive = new GDriveController();

  let body = { 'dropbox' : {}, 'google' : {} };

  dropbox.space(req,res,function(value){
    nbAsyncFinished++;

    body.dropbox.usedSpace = value.used;
    body.dropbox.allocatedSpace = value.allocation.allocated;

    eventEmitter.emit('space',res,body);
  });

  drive.space(req,res,function(value){
    nbAsyncFinished++;

    body.google.usedSpace = parseInt(value.quotaBytesUsed);
    body.google.allocatedSpace = parseInt(value.quotaBytesTotal);

    eventEmitter.emit('space',res,body);
  });

});

function spaceCallback(res,body){
  if(nbAsyncFinished == 2){
    res.json(body);
    nbAsyncFinished = 0;
  }
}


module.exports = router;
