const express = require('express');
const fs = require('fs');
const router = express.Router();
//const querystring = require('querystring');
const request = require('request');

const DropboxController = require('../../controllers/DropboxController');
const GDriveController = require('../../controllers/GDriveController');
const CloudController = require('../../controllers/CloudController');


/** download a file PARAMS = path */
router.get('/',function(req,res){
  let dropbox_path = req.query.dropbox;
  let google_path = req.query.google;

  if (dropbox_path) {
    let dropbox = new DropboxController();
    dropbox.download(dropbox_path, req, res, (body) => {
      console.log('download file dropbox resp ...');
      res.json(body);
    });
  }

  if (google_path) {
    let google = new GDriveController();
    google.download(google_path, req, res, (body) => {
      console.log('download file  gdrive resp ...');
      res.json(body);
    });
  }
});


module.exports = router;
