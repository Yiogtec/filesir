const express = require('express');
const router = express.Router();
const URL = require('url');
//const querystring = require('querystring');
const request = require('request');

const DropboxController = require('../../controllers/DropboxController');
const GDriveController = require('../../controllers/GDriveController');
const CloudController = require('../../controllers/CloudController');


/** create a folder PARAMS = dropbox */
router.post('/',function(req,res){
  let dropbox_path = req.body.dropbox + '/' + req.body.name;
  let google_parent = req.body.google;

  if (req.body.dropbox) {
    let dropbox = new DropboxController();
    dropbox.createFolder(dropbox_path, req, res, (body) => {
      console.log('create folder Dropbox resp ...');
      res.json(body);
    });
  }
  else if (google_parent) {
    let google = new GDriveController();
    google.createFolder(google_parent, req.body.name, req, res, (body) => {
      console.log('create folder Dropbox resp ...');
      res.json(body);
    });
  }
});


module.exports = router;
