const express = require('express');
const router = express.Router();

/* Dropbox ids */
// const client_id ="as7gftzot8a5hyh";
// const client_secret ="riias5a1r6ln4pa";
// const redirect_uri = "http://localhost:8080/dropbox/code";


/* route : /api/cloud/ */
router.use('/content',require('./content'));
router.use('/create',require('./create'));
router.use('/delete',require('./delete'));
router.use('/download',require('./download'));
router.use('/edit',require('./edit'));
router.use('/move',require('./move'));
router.use('/share',require('./share'));
router.use('/upload',require('./upload'));
router.use('/space',require('./space'));



module.exports = router;
