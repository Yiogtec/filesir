const express = require('express');
const router = express.Router();
//const querystring = require('querystring');
const request = require('request');
const objectAssign = require('object-assign');

const DropboxController = require('../../controllers/DropboxController');
const GDriveController = require('../../controllers/GDriveController');
const CloudController = require('../../controllers/CloudController');

/** move file PARAMS = fromPath toPath */
router.patch('/',function(req,res){
  let toMove = req.body.toMove;
  let target = req.body.targetLocation;
  let responseBody = {typeFromFilesir: toMove.type};
  let asyncDown = 0;

  if(toMove.dropbox && (target.dropbox || target.dropbox === '')){
    var name = toMove.dropbox.substring(toMove.dropbox.lastIndexOf('/'));
    let dropbox = new DropboxController();

    dropbox.move(toMove.dropbox, target.dropbox+name, req, res,(body) => {
      asyncDown++;
      responseBody = objectAssign(responseBody, body);
      if(asyncDown == 2) {
        delete body.id;
        res.json(responseBody);
      }
    });

  } else {
    asyncDown++;
    if(asyncDown == 2) {
      res.json(responseBody);
    }
  }

  if(toMove.google && target.google){
    let google = new GDriveController();
    google.move(toMove.google, toMove.parent.google, target.google, req, res, (body) =>{
      asyncDown++;
      responseBody = objectAssign(responseBody ,body);
      if(asyncDown == 2) {
        res.json(responseBody);
      }
    });
  } else {
    asyncDown++;
    if (asyncDown == 2) {
      res.json(responseBody);
    }
  }

});


module.exports = router;
