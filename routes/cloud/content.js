const express = require('express');
const router = express.Router();

const DropboxController = require('../../controllers/DropboxController');
const GDriveController = require('../../controllers/GDriveController');
const CloudController = require('../../controllers/CloudController');


var merge = function(res, dropbox_body, google_body) {
  let cloud = new CloudController();
  json = cloud.mergeContent({dropbox: dropbox_body, google: google_body});
  res.json(json);
  res.end();
}

router.get('/',function(req,res){
  var json;
  var nbAsyncFinished = 0;
  var dropbox_body;
  var google_body;

  let dropbox_path = req.query.dropbox;
  let google_path = req.query.google;

  if(dropbox_path != undefined){
    let dropbox = new DropboxController();
    dropbox.getContent(dropbox_path, req,res, (value) => {
      dropbox_body = value;
      nbAsyncFinished++;
      if (google_path === undefined || nbAsyncFinished === 2) {
        merge(res, dropbox_body, google_body); // defined before the route
      }

    });
  }

  if(google_path != undefined){
    let google = new GDriveController();
    google.getContent(google_path, req,res, (value) => {
      google_body = JSON.parse(value);
      nbAsyncFinished++;
      if (dropbox_path === undefined || nbAsyncFinished === 2) {
        merge(res, dropbox_body, google_body);
      }
    });
  }
});

module.exports = router;
