const express = require('express');
const router = express.Router();
const GDriveController = require('../../controllers/GDriveController');
const google = new GDriveController();

router.get('/', function(req,res){
  google.getAccount(req,res);
});

module.exports = router;
