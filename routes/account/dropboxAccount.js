const express = require('express');
const router = express.Router();
const DropboxController = require('../../controllers/DropboxController');
const dropbox = new DropboxController();

router.get('/', function(req,res){
  dropbox.getAccount(req,res);
});

module.exports = router;
