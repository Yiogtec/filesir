const express = require('express');
const router = express.Router();
const tools = require('../tools');

function handlerDropboxCookie(req,res,next){
  let results = tools.handlerDropboxCookie(req,res);
  if(results.status){
    tools.authError(res,results);
    return;
  }
  next();
};

function handlerGDriveCookie(req,res,next){
  let results = tools.handlerGDriveCookie(req,res);
  if(results.status){
    tools.authError(res,results);
    return;
  }
  next();
};

function checkGoogleToken(req,res,next){
  tools.checkGoogleToken(req,res, next);
}

router.use('/dropbox', handlerDropboxCookie);
router.use('/dropbox', require('./dropboxAccount'));

router.use('/gDrive', handlerGDriveCookie);
router.use('/gDrive', checkGoogleToken);
router.use('/gDrive', require('./gDriveAccount'));

module.exports = router;
