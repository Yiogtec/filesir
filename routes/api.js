const express = require('express');
const router = express.Router();
const tools = require('./tools');


function handlerCookies(req, res, next){
  var results = {};
  results = tools.handlerDropboxCookie(req,res);
  results = tools.handlerGDriveCookie(req,res,results);

  if(results.status){
      tools.authError(res,results.platforms,results.message);
      return;
  }

  next();
};

function checkGoogleToken(req,res,next){
  tools.checkGoogleToken(req,res, next);
}

router.use('/connect',require('./connect/connector_routes'))

router.use('/account', require('./account/account_routes'));

/* Check if cookies are set  */
router.use('/cloud',handlerCookies);
router.use('/cloud',checkGoogleToken);
router.use('/cloud',require('./cloud/cloud_routes'));

module.exports = router;
