const request = require('request');

exports.handlerDropboxCookie = function (request,response,results = {}){
  if(!results.platforms){
    results.platforms = [];
  }

  let dropboxCookie = request.cookies.dropboxCookie;
  if(dropboxCookie === undefined){
    results.status = 401;
    results.platforms.push('Dropbox');
    results.message = "Need to be authenticated";
  }
  return results;
}

exports.handlerGDriveCookie = function (request,response, results = {}){
  if(!results.platforms){
    results.platforms = [];
  }

  let googleDriveCookie = request.cookies.googleDriveCookie;
  if(googleDriveCookie === undefined){
    results.status = 401;
    results.platforms.push('Google Drive');
    results.message = "Need to be authenticated";
  }
  return results;
}

exports.internalError = function (res,platforms,method,body){
  var template = {platforms : []};
  res.status(500);
  template.status = 500;
  template.platforms = platforms;
  template.method = method;
  template.details = body;
  console.log(template);
  res.json(template);
  res.end();
}

exports.authError = function (res,platforms,method){
  var template = {platforms : []};
  res.status(401);
  template.status = 401;
  template.platforms = platforms;
  template.message = method;
  res.json(template);
  res.end();
}

exports.checkGoogleToken = function(req,res,next){
  let token = req.cookies.googleDriveCookie.access_token;

  let headers = {
    "Content-Type": "application/json",
  }
  request.get({
    url:'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token='+token,
    headers: headers
  },function(err,response,body){
      if(!err && (response.statusCode == 200)){
        body = JSON.parse(body);
        if(!body.access_type || body.access_type != 'online'){
          exports.checkGoogleTokenCallback(res, next,{status : 401} );
          return;
        }
      } else if(!err && (response.statusCode == 400)){
        exports.checkGoogleTokenCallback(res,next, {status : 401} );
        return;
      } else {
        body.statusCode = response.statusCode;
        exports.checkGoogleTokenCallback(res,next, {status : 500, details : body} );
        return;
      }
      exports.checkGoogleTokenCallback(res,next,{status : 200});
      return;
  });
}

exports.checkGoogleTokenCallback = function (res,next,results){
  if(results.status == 401){
      res.clearCookie('googleDriveCookie');
      console.log('Google token outdated, so cookies are deleted');
      exports.authError(res,['Google Drive'], 'Need to be authenticated');
      return;
  }
  if(results.status == 500){
      exports.internalError(res,['API Middleware'], 'checkGoogleTokenCallback',results.details );
      return;
  }
  next();
}
