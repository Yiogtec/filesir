const express = require('express');
const router = express.Router();
const querystring = require('querystring');
const request = require('request');
const DropboxController = require('../../controllers/DropboxController');
const tools = require('../tools');

const client_id ="as7gftzot8a5hyh";
const client_secret ="riias5a1r6ln4pa";
const redirect_uri = "http://localhost:8080/api/connect/dropbox/code";


router.get('/', function(req,res){
  if(req.cookies.dropboxCookie) {
    res.redirect('/api/account/dropbox');
    return;
  }else{
    let params = querystring.stringify({
      response_type:'code',
      client_id: client_id,
      redirect_uri:redirect_uri
    });

      res.json({authUrl:'https://www.dropbox.com/oauth2/authorize?'+params});
    }
});

router.get('/code', function(req,res){
  let token = (req.query.code)? req.query.code : null;
  let access_denied = (req.query.error)? req.query.error : null;

  if(access_denied){
    //TODO
  }

  let params = {
    code : token,
    grant_type:"authorization_code",
    client_id : client_id,
    client_secret: client_secret,
    redirect_uri:redirect_uri
  };
  request.post({
      url:'https://api.dropboxapi.com/oauth2/token',
      form: params
    },function(err,response,body){
      if(!err && (response.statusCode == 200)){
        let access_token = JSON.parse(body);
        res.cookie('dropboxCookie',access_token,{httpOnly:true});
        console.log('dropbox cookie created')
        res.redirect('/');
      }else{
        tools.internalError(res,'Dropbox','connect',body);
      }
  });
});

router.get('/logout',function(req,res){
  res.clearCookie('dropboxCookie');
  res.redirect('/');
});

module.exports = router;
