const express = require('express');
const router = express.Router();

router.use('/dropbox', require('./dropboxConnector'));
router.use('/gDrive', require('./gDriveConnector'));

module.exports = router;
