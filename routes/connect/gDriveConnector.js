const express = require('express');
const router = express.Router();
const querystring = require('querystring');
const request = require('request');
const tools = require('../tools');

const client_id = "781232934229-jbvdl4hpmk35obideauq2k0khs0u519v.apps.googleusercontent.com";
const client_secret = "8pe0RT6GSIJ8Z_MzT01L-Ejm";
const redirect_uri = 'http://localhost:8080/api/connect/gDrive/code';

router.get('/', function(req,res){
  if(req.cookies.googleDriveCookie){
    res.redirect('/api/account/gDrive');
    return;
  }else{
    let params = querystring.stringify({
      response_type:'code',
      client_id: client_id,
      scope: 'https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/drive',
      redirect_uri:redirect_uri
    });


    res.json({authUrl:'https://accounts.google.com/o/oauth2/v2/auth?'+params});
  }
});

router.get('/code', function(req,res){
  let token = (req.query.code)? req.query.code : null;

  let params = {
    code : token,
    grant_type:"authorization_code",
    client_id : client_id,
    client_secret: client_secret,
    redirect_uri:redirect_uri
  };
  request.post({
      url:'https://www.googleapis.com/oauth2/v4/token',
      form: params
    },function(err,response,body){
      if(!err && (response.statusCode == 200)){
        let access_token = JSON.parse(body);
        res.cookie('googleDriveCookie',access_token,{httpOnly:true});
        res.redirect('/');
      }else{
        tools.internalError(res,'Google Drive', 'connect',body);
      }
  });
});

router.get('/logout',function(req,res){
  res.clearCookie('googleDriveCookie');
  res.redirect('/');
});

module.exports = router;
