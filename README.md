#Routes

Start project : npm install && npm start // Wait for ng build to complete

The application is listenning to localhost:8080

All routes returns JSON

###Connection###
* /api/connect/dropbox
* /api/connect/dropbox/logout -> redirect /
* /api/connect/gDrive
* /api/connect/gDrive/logout -> redirect /


###Account infos###
* /api/account/dropbox
* /api/account/gDrive
** Infos are encrypted in JWT format


###Cloud###
  * /api/cloud/content
      params : dropbox and google
  * /api/cloud/create
      params : dropbox and google
  * /api/cloud/delete
      params : dropbox and google
  * /api/cloud/move
      params : dropbox_to_path, dropbox_from_path and google
  * /api/cloud/upload
  * /api/cloud/download
  * /api/cloud/share

  ###Auth errors###

  * format :

  {platforms : [], status : 401, message : "Need to be authenticated"}
  {platforms : [], status : 500, method : [Dropbox | Google Drive], details : ""}
