const express = require('express');
const router = express.Router();
const app = express();
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser')



app.set('port',8080);
//app.set('trust proxy', 1) // trust first proxy
app.set('views', path.join(__dirname, 'dist'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.use(router);
app.use(cookieParser());
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'dist')));


app.get('/',function(req,res){
    res.render('index');
});

app.use('/api',require('./routes/api'));

//Handle all others routes (ie. angular routes)
app.use(function(req,res){
    res.sendFile(__dirname + '/dist/index.html');
});



app.listen(app.get('port'));
module.exports = app;
