
/**
 * Created by Anthony on 13/04/2017.
 */

export class DriveKeysModel {

  constructor(public dropbox, public google, public others: {drive: string, value: string}[] = []) { }

  get(): Object {
    const keys = { dropbox: this.dropbox, google: this.google };
    this.others.forEach((key) => keys[key.drive] = key.value);
    return keys;
  }
}
