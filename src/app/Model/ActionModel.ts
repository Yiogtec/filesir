import {FolderModel} from "./FolderModel";
/**
 * Created by Yiogtec on 18/04/2017.
 */

export class ActionModel {

  constructor(public name: string, public icon: string, private env = null) { }

  execute() {
    this.env[this.name]();
  }
}
