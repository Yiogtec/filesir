import {AbstractFileModel} from "./AbstractFileModel";
import {DriveKeysModel} from "./DriveKeysModel";
import {FileActionService} from "../providers/file-action.service";
/**
 * Created by Anthony on 07/04/2017.
 */

export class FolderModel extends AbstractFileModel {

  error: string;
  success: string;

  constructor(
    name: string, path: string,
    public keys: DriveKeysModel = new DriveKeysModel('', 'root'),
    public modifiedDate: Date = null,
    public shared: boolean = false,
    public parent: FolderModel = null
  ) { super(name, path, keys, modifiedDate, shared, parent); }

  getKeys(): any {
    return this.keys.get();
  }
  type(): string {
    return 'FolderModel';
  }
}
