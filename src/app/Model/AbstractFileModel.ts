import {FileActionService} from "../providers/file-action.service";
import {DriveKeysModel} from "./DriveKeysModel";
import {FolderModel} from "./FolderModel";
/**
 * Created by Anthony on 07/04/2017.
 */

export abstract class AbstractFileModel {

  success: string;
  error: string;
  display: boolean = true;

  constructor(
    public name: string,
    private path: string = '/',
    public keys: DriveKeysModel,
    public modifiedDate: Date,
    public shared: boolean,
    public parent: FolderModel) { }

  abstract type(): string;
  abstract getKeys(): any;

  delete(provider: FileActionService) {
    this.success = null;
    provider.delete(this).subscribe(
      (res) => {
        this.display = false;
        this.success = 'Element has been deleted';
      },
      (error) => this.error = error.message.toString()
    );
  }

  edit(newName: string, provider: FileActionService) {
    this.success = null;
    this.error = null;
    if (newName === undefined || newName === '') {
      this.error = 'Error ! Empty value';
    } else {
      provider.edit(this, newName).subscribe(
        (res) => {
          this.name = res.name;
          this.setDropboxKey(res.path_lower);
          // TODO google rename, auto ? the id doesn't change ?
          this.success = 'Element has been renamed to ' + newName;
        },
        (error) => this.error = error.message.toString()
      );
    }
  }

  share(info: {dropboxUserEmail: string, googleUserEmail: string}, provider: FileActionService) {
    this.success = null;
    this.error = null;
    provider.share(this, info).subscribe(
      (res) => {
        this.shared = true;
        this.success = 'Shared has been done';
      },
      (error) => this.error = error.message.toString()
    );
  }

  setDropboxKey(newKey: string) {
    this.keys.dropbox = newKey;
  }

  setGoogleKey(newKey: string) {
    this.keys.google = newKey;
  }

  getPath(): string {
    return this.path + this.name; // care to '/' at the end of path
  }

}
