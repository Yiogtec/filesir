import {AbstractFileModel} from "./AbstractFileModel";
import {DriveKeysModel} from "./DriveKeysModel";
import {FolderModel} from "./FolderModel";
/**
 * Created by Anthony on 07/04/2017.
 */

export class FileModel extends AbstractFileModel {

    constructor(
      name: string, path: string = '/',
      private driveKey: DriveKeysModel,
      public modifiedDate: Date = null,
      public shared: boolean = false,
      public size: number,
      public parent: FolderModel = null
    ) {
        super(name, path, driveKey, modifiedDate, shared, parent);
        this.size = this.size ? this.size /= 1000 : 0; // TODO should be a pipe
    }

  download(provider) {
    provider.download(this).subscribe(
      (resp) => {
        const link : any  = document.getElementById('download-file-link');
        link.href = resp.link;
        link.click();
      }
    );
  }

  getKeys(): any {
    return this.driveKey.get();
  }
  type(): string {
    return 'File';
  }

}
