/**
 * Created by Yiogtec on 27/04/2017.
 */

import { Pipe, PipeTransform } from '@angular/core';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 |  exponentialStrength:10}}
 *   formats to: 1024
 */
@Pipe({name: 'display', pure: false})
export class DisplayPipe implements PipeTransform {

  transform(content, contentFilter) {
    const res = content.sort(this.compareByTarget(contentFilter.target));
    return contentFilter.order === 'asc' ? res : res.reverse();
  }
  compareByTarget(key) {
    return function (a, b) {
      let toCompareA;
      let toCompareB;
      if (key === 'name') {
        toCompareA = a[key].toLowerCase();
        toCompareB = b[key].toLowerCase();
        if (toCompareA < toCompareB) {
          return -1;
        } else if (toCompareA > toCompareB) {
          return 1;
        }
        return 0;
      } else if (key === 'modifiedDate') {
          if (!a[key]) {
            return -1;
          }else if (!b[key]) {
            return 1;
          };

          if (a[key].getTime() < b[key].getTime()) {
            return -1;
          } else if (a[key].getTime() > b[key].getTime()) {
            return 1;
          }
          return 0;
      } else {
        if (a[key] < b[key]) {
          return -1;
        } else if (a[key] > b[key]) {
          return 1;
        }
        return 0;
      }
    };
  }
}

