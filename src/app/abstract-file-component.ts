import {ActionModel} from "./Model/ActionModel";
import {EventEmitter, Input, Output, ViewChild} from "@angular/core";
import {ActionComponent} from "./Component/action/action.component";
import {AbstractFileModel} from "./Model/AbstractFileModel";
import {FileActionService} from "./providers/file-action.service";
/**
 * Created by Yiogtec on 30/04/2017.
 */

export abstract class AbstractFileComponent {

  @ViewChild(ActionComponent) actionComponent;
  @Input() element: AbstractFileModel;
  actions: ActionModel[];
  activeAction: string;
  @Output() notifyCut: EventEmitter<Object> = new EventEmitter<Object>();

  constructor(private provider: FileActionService) {
    this.actions = [];
    this.actions.push(new ActionModel('share', 'share', this));
    this.actions.push(new ActionModel('cut', 'content_cut', this));
    this.actions.push(new ActionModel('edit', 'edit', this));
    this.actions.push(new ActionModel('delete', 'delete', this));
  }


  share() {
    this.cleanMessages();
    this.actionComponent.activeAction = this.actionComponent.activeAction === 'share' ? null : 'share';
  }

  move() {
    this.cleanMessages();
    this.actionComponent.activeAction = this.actionComponent.activeAction === 'move' ? null : 'move';
  }

  delete() {
    this.cleanMessages();
    this.actionComponent.activeAction = this.actionComponent.activeAction === 'delete' ? null : 'delete';
  }

  edit() {
    this.cleanMessages();
    this.actionComponent.activeAction = this.actionComponent.activeAction === 'edit' ? null : 'edit';
  }

  doDelete() {
    this.element.delete(this.provider);
    this.actionComponent.activeAction = null;
  }

  cut() {
    this.cleanMessages();
    this.actionComponent.activeAction = null;
    this.notifyCut.emit(this.element);
  }

  protected cleanMessages() {
    this.element.error = null;
    this.element.success = null;
  }

  setElement(elem: AbstractFileModel) {
    this.element = elem;
  }

}
