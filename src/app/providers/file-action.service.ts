import { Injectable } from '@angular/core';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import {Observable} from 'rxjs';
import {FolderModel} from '../Model/FolderModel';
import { NodeRequesterProvider } from './NodeRequester';
import {Router} from "@angular/router";
import {LoggingComponent} from "../Component/logging/logging.component";
import {AbstractFileModel} from "../Model/AbstractFileModel";
import {FileModel} from "../Model/FileModel";

@Injectable()
export class FileActionService extends NodeRequesterProvider {

  public static staticRouter;
  constructor(private http: Http, private router: Router) { super(); FileActionService.staticRouter = this.router; }
  getContent(folder: FolderModel): Observable<any> {
    return this.http.get(this.host + '/cloud/content', { params: folder.getKeys() })
                    .map(this.simpleExtract)
                    .catch(this.handleContentError);
  }

  private simpleExtract(res: Response) {
    const body = res.json();
    return body;
  }

  private handleContentError(error: any | Response) {
    if (error.status === 401) {
      LoggingComponent.error = 'You have been disconnected from a cloud, plrease loggin again.';
      FileActionService.staticRouter.navigate(
        ['/']
      );
      {
        return Observable.throw('You have been disconnected from a cloud, plrease loggin again.');
      }
    }else {
      throw new Error(error);
    }
  }

  create(folder: FolderModel, newName: string, platform: string = 'dropbox'): Observable<any> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    const params = folder.getKeys();
    params.name = newName;
    params.platform = platform;
    console.log(params.dropbox);

    return this.http.post(this.host + '/cloud/create', params, options)
                      .map(this.simpleExtract)
                      .catch(this.handleContentError);
  }

  edit(file: AbstractFileModel, newName: string) {
    const params = file.getKeys();
    params.name = newName;

    return this.http.put(this.host + '/cloud/edit', params)
                    .map(this.simpleExtract)
                    .catch(this.handleContentError);
  }

  delete(file: AbstractFileModel) {
    const params = file.getKeys();
    const headers: Headers = new Headers();
    headers.append('target-to-delete', JSON.stringify(params));

    return this.http.delete(this.host + '/cloud/delete', { headers : headers })
      .map(this.simpleExtract)
      .catch(this.handleContentError);
  }

  upload(newFile, name: string, containerFolder: FolderModel, platform: string, callback) {
    // xhr way to do it
    const xhr = new XMLHttpRequest();
    const formData = new FormData();
    const keys = containerFolder.getKeys();
    const self = this;

    xhr.onreadystatechange = function(e) {
      if (XMLHttpRequest.DONE === this.readyState) {
          if (200 === this.status) {
            callback(JSON.parse(xhr.responseText));
          } else {
            callback(null, e);
          }
      }
    };
    xhr.open('post', this.host + '/cloud/upload', true);
    // xhr.setRequestHeader('Content-Type', 'multipart/form-data');
    formData.append('dropbox', keys.dropbox);
    formData.append('google', keys.google);
    formData.append('name', name);
    formData.append('platform', platform);
    formData.append('filedata', newFile);
    xhr.send(formData);

    /*const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    const keys = containerFolder.getKeys();
    const params = { dropbox: keys.dropbox,
                     google: keys.google,
                     name: name,
                     platform: platform,
                     fileData: newFileBinary
                  };

    return this.http.post(this.host + '/cloud/upload', params, options)
      .map(this.simpleExtract)
      .catch(this.handleContentError);*/
  }

  download(file: FileModel) {
    return this.http.get(this.host + '/cloud/download', { params: file.getKeys() })
      .map(this.simpleExtract)
      .catch(this.handleContentError);
  }

  share(elem: AbstractFileModel, info: {dropboxUserEmail: string, googleUserEmail: string}) {
    const params = elem.getKeys();
    params.dropboxUserEmail = info.dropboxUserEmail;
    params.googleUserEmail = info.googleUserEmail;
    return this.http.patch(this.host + '/cloud/share', params)
      .map(this.simpleExtract)
      .catch(this.handleContentError);
  }

  move(toMove: AbstractFileModel, target: AbstractFileModel) {
    const params = { toMove: toMove.getKeys(), targetLocation: target.getKeys() };
    params.toMove.parent = toMove.parent.getKeys();
    params.toMove.type = toMove.type();
    return this.http.patch(this.host + '/cloud/move', params)
      .map(this.simpleExtract)
      .catch(this.handleContentError);
  }

  extractNewFolder(res: Response) {
    const body = res.json();
    return body;
  }
}
