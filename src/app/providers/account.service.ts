import { Injectable } from '@angular/core';
import {Http, Response} from "@angular/http";
import { NodeRequesterProvider } from "./NodeRequester";
import {Observable} from "rxjs";
import {LoggingComponent} from "../Component/logging/logging.component";

@Injectable()
export class AccountService extends NodeRequesterProvider {

  constructor(private http: Http) { super(); }

  getAccount(drive: string): Observable<any> {
    return this.http.get(this.host + '/account/' + drive)
                    .map(this.extractAccount)
                    .catch(this.handleAccountError);
  }

  getSpace() {
    return this.http.get(this.host + '/cloud/space')
                    .map(this.extractSpace)
                    .catch(this.handleAccountError);
  }

  private extractSpace(res: Response) {
    const body = res.json();
    console.log(body);
    return body;
  }
  private extractAccount(res: Response) {
    LoggingComponent.error = null;
    const body = res.json();
    return body;
  }

  private handleAccountError(error: Response | any ) {
    console.log('an error occured');
    if (error instanceof Response) {
      const body = error.json();
      return body;
    } else {
      return error.message;
    }
  }

  connect(drive: string): Observable<any> {
    return this.http.get(this.host + '/connect/' + drive)
                    .map((res) => {return res.json()});
  }




}
