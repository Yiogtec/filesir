import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriveAccountComponent } from './drive-account.component';

describe('DriveAccountComponent', () => {
  let component: DriveAccountComponent;
  let fixture: ComponentFixture<DriveAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriveAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriveAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
