import {Component, Input, OnInit} from '@angular/core';
import {AccountService} from '../../providers/account.service';
import {JwtHelper} from "angular2-jwt";


@Component({
  selector: 'app-drive-account',
  templateUrl: './drive-account.component.html',
  styleUrls: ['./drive-account.component.css']
})
export class DriveAccountComponent implements OnInit {

  @Input('drive') drive: string;

  account: any;
  error: any;
  authUrl: string;
  jwtHelper: JwtHelper = new JwtHelper();


  constructor(private accountProvider: AccountService) {
  }

  getAccount() {
    this.accountProvider.getAccount(this.drive)
      .subscribe(
        account => this.account = account,
        error => this.error = error
      );
  }

  connect() {
    var self = this;
    this.accountProvider.connect(this.drive)
                        .subscribe(
                          response => {
                            if (response.authUrl) {
                              self.authUrl = response.authUrl;
                              self.account = null;
                            } else {
                              self.account = this.drive === 'gDrive' ? response.displayName : response.name.given_name;
                            }
                          }
                        );
  }

  ngOnInit() {
    this.connect();
  }

}
