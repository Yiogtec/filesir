import {Component, OnInit} from "@angular/core";
import {FileActionService} from "../../providers/file-action.service";
import {AccountService} from "../../providers/account.service";
import {FolderModel} from "../../Model/FolderModel";
import {FolderComponent} from "../folder/folder.component";
import {AbstractFileModel} from "../../Model/AbstractFileModel";
import {DriveKeysModel} from "../../Model/DriveKeysModel";
import {FileModel} from "../../Model/FileModel";

@Component({
  selector: 'app-explorer',
  templateUrl: './explorer.component.html',
  styleUrls: ['./explorer.component.css']
})
export class ExplorerComponent implements OnInit {

  folders:  FolderModel[];
  files: FileModel[];
  navigation: FolderModel[]; // a stack of folders
  cutItem: AbstractFileModel = null;
  itemSelector: any; // FolderModel | FileModel doesn't seem to work
  success: string;
  error: string;
  fileToUpload: any;
  driveSpace: any;

  private contentFilter: {target: string, order: 'desc' | 'asc'};

  constructor(public fileActionsProvider: FileActionService, public accountProvider: AccountService) {
    this.folders = [];
    this.files = [];
    this.navigation = [];

    this.contentFilter = { target: 'name', order: 'asc' };
    // request for all data
    // init a root directory
    this.navigation.push(new FolderModel('', 'root'));
    this.refresh();
    this.getSpace();
  }

  current(): FolderModel {
    return this.navigation[this.navigation.length - 1];
  }

  push(folder: FolderModel) {
    this.navigation.push(folder);
    this.refresh();
  }

  pop() {
    this.navigation.pop();
    this.refresh();
  }

  setNavigation(folderPlace: number) {
    // erase the stack until the element which has been click (length - 1 - place in reverse stack)
    this.navigation.splice(folderPlace + 1, this.navigation.length);
    // navigation changed then request for all data
    this.refresh();
  }

  refresh() {
    this.success = null;
    this.error = null;
    this.setContent(this.navigation[this.navigation.length - 1]);
  }

  setContent(folder: FolderModel) {
    this.itemSelector = null;
    this.folders = [];
    this.files = [];

    this.fileActionsProvider.getContent(folder).subscribe(
      (data) => {
        data.folders.forEach( (folder) => {
          if (folder.labels ? ! folder.labels.trashed : true) {
            this.folders.push(new FolderModel(
              folder.name,
              folder.path_lower,
              new DriveKeysModel(folder.path_lower, folder.google_id, [{drive: 'dropboxId', value: folder.dropbox_id}]),
              folder.modifiedDate ? new Date(folder.modifiedDate) : null, // TODO prendre la plus récente
              folder.shared ? folder.shared : folder.sharing_info,
              this.current()
              )
            );
          }
        });
        data.files.forEach(
          (file) => {
            if (file.labels ? ! file.labels.trashed : true) {
              const driveKey: DriveKeysModel = new DriveKeysModel(file.path_lower, file.id, [{drive: 'dropboxId', value: file.dropbox_id}]); // one should be undefined
              this.files.push(new FileModel(
                file.name,
                file.path_lower, driveKey,
                file.modifiedDate ? new Date(file.modifiedDate) : new Date(file.server_modified),
                file.shared ? file.shared : file.has_explicit_shared_members,
                file.fileSize ? file.fileSize : file.size,
                this.current()
              ));
            }
          }
        );
      });
  }

  createFolder(newName: string) {
    this.success = null;
    this.error = null;
    this.fileActionsProvider.create(this.current(), newName, 'dropbox').subscribe(
      (folder) => {
        const driveKeys = new DriveKeysModel(folder.path_lower, folder.google_id, [{drive: 'dropboxId', value: folder.dropbox_id}]);
        this.folders.push(new FolderModel(folder.name, folder.path_lower, driveKeys, folder.modifiedDate));
        this.success = 'Folder ' + folder.name + ' has been created';
      },
      (error) => this.error = error.message.toString()
    );
  }

  onChangeFileUpload(event) {
    const files = event.target.files || event.srcElement.files;
    this.fileToUpload = files[0];
  }

  uploadFile(platform: string) {
    const self = this;
    if (this.fileToUpload) {
      const callback = function(fileResp, e) {
        if (fileResp) {
          const driveKey: DriveKeysModel = new DriveKeysModel(fileResp.path_lower, fileResp.id, [{drive: 'dropboxId', value: fileResp.dropbox_id}]);
          const newFile: FileModel = new FileModel(
            fileResp.name,
            fileResp.path_lower,
            driveKey,
            fileResp.modifiedDate ? new Date(fileResp.modifiedDate) : new Date(fileResp.server_modified),
            fileResp.shared,
            fileResp.fileSize ? fileResp.fileSize : fileResp.size,
            self.current()
          );
          self.files.push(newFile);
        } else {
          self.error = e;
        }
      }

      self.fileActionsProvider.upload(this.fileToUpload, this.fileToUpload.name, self.current(), platform, callback);
    }
  }

  moveItem() {
    this.fileActionsProvider.move(this.cutItem, this.current()  ).subscribe(
      (res) => {
        this.success = 'The element has been move';
        this.cutItem = null;
        if (res.typeFromFilesir === 'FileModel') {
          this.files.push(new FileModel(
            res.name,
            res.path_lower,
            res,
            res.modifiedDate ? new Date(res.modifiedDate) : new Date(res.server_modified),
            res.shared,
            res.fileSize ? res.fileSize : res.size,
            this.current()
          ));
        } else if (res.typeFromFilesir === 'FolderModel') {
          this.folders.push(new FolderModel(
              res.name,
              res.path_lower,
              new DriveKeysModel(res.path_lower, res.google_id, [{drive: 'dropboxId', value: res.dropbox_id}]),
              res.modifiedDate ? new Date(res.modifiedDate) : null, // TODO prendre la plus récente
              res.shared ? res.shared : res.sharing_info,
              this.current()
            )
          );
        }
      },
      (error) => this.error = error.message.toString()
    );
  }

  canPaste(): boolean {
    const currentIsDropbox = this.current().getKeys().dropbox;
    const currentIsGoogle = this.current().getKeys().google;
    const toPasteIsDropbox = this.cutItem.getKeys().dropbox;
    const toPasteIsGoogle = this.cutItem.getKeys().google;

    const res: boolean = (toPasteIsDropbox && (currentIsDropbox || currentIsDropbox === '')) || (toPasteIsGoogle && currentIsGoogle);
    return res;
  }
  setContentFilter(filterName: string) {
    this.contentFilter = {
      target: filterName,
      order: this.contentFilter.order === 'asc' ? 'desc' : 'asc' // toggle order
    };
  }

  getSpace() {
    this.accountProvider.getSpace().subscribe(
      (resp) => {
        this.driveSpace = resp;
        const pourcent = this.getPourcentUsedSpace();
        this.driveSpace.dropbox.pourcent = pourcent.dropbox;
        this.driveSpace.google.pourcent = pourcent.google;
      }
    );
  }

  getPourcentUsedSpace(): any {
    const dropbox = this.driveSpace.dropbox.usedSpace / this.driveSpace.dropbox.allocatedSpace * 100;
    const google = this.driveSpace.google.usedSpace / this.driveSpace.google.allocatedSpace * 100;
    return {
      dropbox: dropbox > 1 ? dropbox : 1,
      google: google > 1 ? google : 1
    };
  }
  getContentFilter() {
    return this.contentFilter;
  }

  ngOnInit() { }
}
