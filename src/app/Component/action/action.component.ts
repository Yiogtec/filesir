import {Component, Input, OnInit} from "@angular/core";
import {AbstractFileModel} from "../../Model/AbstractFileModel";
import {FileActionService} from "../../providers/file-action.service";

@Component({
  selector: 'app-action',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.css']
})
export class ActionComponent implements OnInit {

  @Input() element: AbstractFileModel; // FileModel | FolderModel
  activeAction: string;
  editFileNameInputValue: string;
  inputValues: any = {};
  constructor(public provider: FileActionService) {
    this.inputValues.share = {
      dropboxUserEmail: null,
      googleUserEmail: null
    };
  }

  ngOnInit() {
    this.editFileNameInputValue = this.element.name;
  }

}
