import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {AbstractFileComponent} from "../../abstract-file-component";
import {ActionModel} from "../../Model/ActionModel";
import {FolderModel} from "../../Model/FolderModel";
import {FileActionService} from "../../providers/file-action.service";

@Component({
  selector: 'app-folder',
  templateUrl: './folder.component.html',
  styleUrls: ['./folder.component.css']
})
export class FolderComponent extends AbstractFileComponent implements OnInit {

  actions: ActionModel[];
  element: FolderModel;


  constructor(private folderProvider: FileActionService) {
      super(folderProvider);
  }

  ngOnInit() {

    // tricky
    if (this.element.getKeys().dropbox !== undefined && !this.element.getKeys().google) {
      this.actions.splice(0, 1);
    }
  }

}
