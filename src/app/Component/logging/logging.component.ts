import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, NavigationEnd, Params} from "@angular/router";
import {getErrorLogger} from "@angular/core/src/errors";

@Component({
  selector: 'app-logging',
  templateUrl: './logging.component.html',
  styleUrls: ['./logging.component.css']
})
export class LoggingComponent implements OnInit {

  static error: string;

  constructor() { }

  ngOnInit() { }

  getError() {
    return LoggingComponent.error;
  }

}
