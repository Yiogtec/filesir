import {Component, OnInit } from '@angular/core';
import {AbstractFileComponent} from "../../abstract-file-component";
import {ActionModel} from "../../Model/ActionModel";
import {FileModel} from "../../Model/FileModel";
import {FileActionService} from "../../providers/file-action.service";

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.css']
})
export class FileComponent extends AbstractFileComponent implements OnInit {

  actions: ActionModel[];
  element: FileModel;

  constructor(public fileActionProvider: FileActionService) {
    super(fileActionProvider);
    this.actions.unshift(new ActionModel('download', 'file_download', this));
  }

  download() {
    this.cleanMessages();
    this.actionComponent.activeAction = null;
    this.element.download(this.fileActionProvider);
  }

  ngOnInit() { }

}
