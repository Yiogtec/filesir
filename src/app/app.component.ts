import { Component } from '@angular/core';
import {FolderComponent} from "./Component/folder/folder.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  private type: string = 'element';
  title = 'app works!';

  constructor() { }
}
