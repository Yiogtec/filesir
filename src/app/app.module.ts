import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { FileComponent } from './Component/file/file.component';
import { FolderComponent } from './Component/folder/folder.component';
import { ExplorerComponent } from './Component/explorer/explorer.component';
import {FileActionService} from './providers/file-action.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from '@angular/material';
import {RouterModule} from '@angular/router';
import { DriveAccountComponent } from './Component/drive-account/drive-account.component';
import {AccountService} from './providers/account.service';
import { LoggingComponent } from './Component/logging/logging.component';
import {DisplayPipe} from './pipes/DisplayPipe';
import { ActionComponent } from './Component/action/action.component';

const appRoutes = [
  { path: '', component: LoggingComponent },
  { path: 'drive', component: ExplorerComponent },
  { path: '**', component : ExplorerComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    FileComponent,
    FolderComponent,
    ExplorerComponent,
    DriveAccountComponent,
    LoggingComponent,
    DisplayPipe,
    ActionComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    MaterialModule, // TODO
    RouterModule.forRoot(appRoutes)
  ],
  providers: [FileActionService, AccountService],
  bootstrap: [AppComponent]
})
export class AppModule { }
