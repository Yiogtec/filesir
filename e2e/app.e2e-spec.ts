import { FilesirFrontPage } from './app.po';

describe('filesir-front App', () => {
  let page: FilesirFrontPage;

  beforeEach(() => {
    page = new FilesirFrontPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
