const request = require('request');
const tools = require('../routes/tools');

var DropboxController = function() {
  //Singleton
  if ( arguments.callee._singletonInstance ) return arguments.callee._singletonInstance;
  arguments.callee._singletonInstance = this;

  this.token = null;
}

DropboxController.prototype.getAccount = function(req,res) {
  let token = req.cookies.dropboxCookie;

  let headers = {
    "Content-Type": "application/json",
    "Authorization":"Bearer "+token.access_token
  };
  let params = {
    "account_id": token.account_id
  };

  request.post({
    url:'https://api.dropboxapi.com/2/users/get_account',
    headers: headers,
    json: params
  },function(err,response,body){
    if(!err && (response.statusCode == 200)){
      res.json(body);
    } else {
      tools.internalError(res,'Dropbox','account',JSON.parse(body));
    }

  });
}

DropboxController.prototype.getContent = function(path, req,res, callback) {
  this.token = req.cookies.dropboxCookie;

  let headers_dropbox = {
    "Content-Type": "application/json",
    "Authorization":"Bearer "+this.token.access_token
  };
  let params_dropbox = {
    "path": path,
    "recursive": false,
    "include_media_info": false,
    "include_deleted": false,
    "include_has_explicit_shared_members": true
  };

  request.post({
    url:'https://api.dropboxapi.com/2/files/list_folder',
    headers: headers_dropbox,
    json: params_dropbox
  }, function(err,response,body){
    if(!err && (response.statusCode == 200)){
      /* fonction passée en parametre de getContent */
      body.dropbox_id = body.id;
      delete body.id;
      callback(body);
    } else {
      tools.internalError(res,'DropBox','getContent',JSON.parse(body));
    }
  });
}

DropboxController.prototype.createFolder = function(path, req,res,callback) {
  this.token = req.cookies.dropboxCookie;

  let headers_dropbox = {
    "Content-Type": "application/json",
    "Authorization":"Bearer "+this.token.access_token
  };
  let params_dropbox = {
    "path": path,
    "autorename": false
  };

  request.post({
    url:'https://api.dropboxapi.com/2/files/create_folder',
    headers: headers_dropbox,
    json: params_dropbox
  }, function(err,response,body){
    if(!err && (response.statusCode == 200)){
      /* fonction passée en parametre de getContent */
      body.dropbox_id = body.id;
      delete body.id;
      callback(body);
    } else {
      tools.internalError(res,'DropBox','create',body);
    }
  });
}

DropboxController.prototype.delete = function(path, req,res,callback) {
  this.token = req.cookies.dropboxCookie;

  let headers_dropbox = {
    "Content-Type": "application/json",
    "Authorization":"Bearer "+this.token.access_token
  };
  let params_dropbox = {
    "path": path
  };

  request.post({
    url:'https://api.dropboxapi.com/2/files/delete',
    headers: headers_dropbox,
    json: params_dropbox
  }, function(err,response,body){
    if(!err && (response.statusCode == 200)){
      /* fonction passée en parametre de getContent */
      callback(body);
    } else {
      tools.internalError(res,'DropBox','delete',body);
    }
  });

}

DropboxController.prototype.move = function(from_path, to_path, req,res,callback) {
  this.token = req.cookies.dropboxCookie;

  let headers_dropbox = {
    "Content-Type": "application/json",
    "Authorization":"Bearer "+this.token.access_token
  };
  let params_dropbox = {
    "from_path": from_path,
    "to_path": to_path,
    "allow_shared_folder": false,
    "autorename": false
  };

  request.post({
    url:'https://api.dropboxapi.com/2/files/move',
    headers: headers_dropbox,
    json: params_dropbox
  }, function(err,response,body){
    if(!err && (response.statusCode == 200)){
      /* fonction passée en parametre de getContent */
      body.dropbox_id = body.id;
      delete body.id;
      callback(body);
    } else {
      tools.internalError(res,'DropBox','move',body);
    }
  });

}

DropboxController.prototype.download = function(path, req,res,callback) {
  this.token = req.cookies.dropboxCookie;

  let params_dropbox = {
    "path": path
  };

  let headers_dropbox = {
    "Authorization":"Bearer "+this.token.access_token,
    "Content-Type": "application/json"
  };

  request.post({
    url:'https://api.dropboxapi.com/2/files/get_temporary_link',
    headers: headers_dropbox,
    json: params_dropbox
  }, function(err,response,body){
    if(!err && (response.statusCode == 200)){
      //  console.log(response.headers)
      callback(body);
    } else {
      tools.internalError(res,'DropBox','download',body);
    }
  });
}

DropboxController.prototype.upload = function(path, fileData, req,res,callback) {
  this.token = req.cookies.dropboxCookie;

  let params_dropbox = {
    "path": path,
    "mode": "add",
    "autorename": true,
    "mute": false
  };

  let headers_dropbox = {
    "Authorization":"Bearer "+ this.token.access_token,
    "Dropbox-API-Arg": JSON.stringify(params_dropbox),
    "Content-Type": "application/octet-stream",
  };

  request.post({
    url:'https://content.dropboxapi.com/2/files/upload',
    headers: headers_dropbox,
    body: fileData,
  }, function(err,response,body) {
    if(!err && (response.statusCode == 200)){
      body = JSON.parse(body);
      body.dropbox_id = body.id;
      delete body.id;
      callback(body);
    } else {
      tools.internalError(res,'DropBox','upload',body);
    }
  });
}

DropboxController.prototype.share = function(id, mail, req,res,callback) {
  this.token = req.cookies.dropboxCookie;

  let headers_dropbox = {
    "Content-Type": "application/json",
    "Authorization":"Bearer "+this.token.access_token
  };
  let params_dropbox = {
    "file": id,
    "members": [
      {
        ".tag": "email",
        "email": mail
      }
    ],
    "custom_message": "This is a custom message about ACME.doc",
    "quiet": false,
    "access_level": "viewer",
    "add_message_as_comment": false
  };

  request.post({
    url:'https://api.dropboxapi.com/2/sharing/add_file_member',
    headers: headers_dropbox,
    json: params_dropbox
  }, function(err,response,body){
    if(!err && (response.statusCode == 200)){
      /* fonction passée en parametre de getContent */
      callback(body);
    } else {
      tools.internalError(res,'DropBox','share',body);
    }
  });

}

DropboxController.prototype.space = function(req,res,callback) {
  this.token = req.cookies.dropboxCookie;

    let headers = {
      "Authorization":"Bearer "+this.token.access_token
    };

    request.post({
      url:'https://api.dropboxapi.com/2/users/get_space_usage',
      headers: headers,
    },function(err,response,body){
      if(!err && (response.statusCode == 200)){
        body = JSON.parse(body);
        callback(body);
      } else {
        tools.internalError(res,'Dropbox','space',body);
      }

    });
}

module.exports = DropboxController;
