/**
 * Created by Anthony on 16/04/2017.
 */
const objectAssign = require('object-assign');

var CloudController = function() {
    //Singleton
    if ( arguments.callee._singletonInstance ) return arguments.callee._singletonInstance;
    arguments.callee._singletonInstance = this;
}

CloudController.prototype.mergeContent = function(unmergeContent) {
  var dropboxClean = {files: [], folders: []};
  var googleClean = {files: [], folders: []};

  let dropboxContent = unmergeContent.dropbox ? unmergeContent.dropbox.entries : [];
  let googleContent = unmergeContent.google ? unmergeContent.google.items : [];

  dropboxContent.forEach( (entry) => {
    entry.dropbox_id = entry.id;
    delete entry.id;
    entry['.tag'] == 'folder' ? dropboxClean.folders.push(entry) : dropboxClean.files.push(entry);
  });
  //console.log(googleContent);
  googleContent.forEach( (entry) => {
    entry.name = entry.title; //tricky
    entry.google_id = entry.id; //tricky
    //console.log(entry.mimeType);
    entry.mimeType == 'application/vnd.google-apps.folder' ? googleClean.folders.push(entry) : googleClean.files.push(entry);
    //console.log(googleClean.folders);
  });

  return {
    folders: this.concatWithMerge(dropboxClean.folders, googleClean.folders, 'name'),
    files: dropboxClean.files.concat(googleClean.files)
  };
}

CloudController.prototype.concatWithMerge = function(arr1, arr2, mergeBasedProperty) {
  if(arr1.length == 0) return arr2;
  if(arr2.length == 0) return arr1;

  arr1.forEach( (itemArr1,indexArr1) => {
    arr2.forEach( (itemArr2,indexArr2) => {
      if (itemArr1[mergeBasedProperty] == itemArr2[mergeBasedProperty]) {
        arr1[indexArr1] = objectAssign(itemArr1 ,itemArr2);
        arr2.splice(indexArr2,1);
      }
    });
  });
  return arr1.concat(arr2);
}

module.exports = CloudController;
