const request = require('request');
const tools = require('../routes/tools');

var GDriveController = function() {
  //Singleton
  if ( arguments.callee._singletonInstance ) return arguments.callee._singletonInstance;
  arguments.callee._singletonInstance = this;

  this.token = null;
}

GDriveController.prototype.getAccount = function(req,res){
  let token = req.cookies.googleDriveCookie;

  let headers = {
    "Content-Type": "application/json",
    "Authorization":"Bearer "+token.access_token
  }

  request.get({
    url:'https://www.googleapis.com/plus/v1/people/me',
    headers: headers
  },function(err,response,body){
    if(!err && (response.statusCode == 200)){
      res.json(JSON.parse(body));
    } else {
      tools.internalError(res,'Google Drive','account',JSON.parse(body));
    }

  });
  console.log('The Google drive token may be expired');
} //endGetAccount

GDriveController.prototype.getContent = function(path, req,res, callback) {
  this.token = req.cookies.googleDriveCookie;
  let headers_google = {
    "Content-Type": "application/json",
    "Authorization":"Bearer "+this.token.access_token
  };
  request.get({
    url:'https://www.googleapis.com/drive/v2/files?q=\''+path+'\' in parents',
    headers: headers_google
  },function(err,response,body){
    if(!err && (response.statusCode == 200)){
      /* fonction passée en parametre de getContent */
      callback(body);
    } else if(response.statusCode == 401){

    } else {
      tools.internalError(res ,'Google Drive','getContent',body );
    }
  });
} //endGetContent

GDriveController.prototype.createFolder = function(parent_id, name_folder, req,res, callback) {
  this.token = req.cookies.googleDriveCookie;
  let headers_google = {
    "Content-Type": "application/json",
    "Authorization":"Bearer "+this.token.access_token
  };

  let body_google = {
    "title": name_folder,
    "parents": [{ "id" : parent_id }],
    "mimeType":"application/vnd.google-apps.folder"
  };

  request.post({
    url: "https://www.googleapis.com/drive/v2/files",
    headers: headers_google,
    body: JSON.stringify(body_google)
  },function(err,response,body){
    if(!err && (response.statusCode == 200)){
      body = JSON.parse(body);
      body.name = body.title;
      body.google_id = body.id;
      callback(body);
    } else{
      tools.internalError(res ,'Google Drive','create_folder',body );
    }
  });
} //endGetContent

GDriveController.prototype.delete = function(path, req,res, callback) {

  this.token = req.cookies.googleDriveCookie;
  let headers_google = {
    "Content-Type": "application/json",
    "Authorization":"Bearer "+this.token.access_token
  };
  request.delete({
    url:'https://www.googleapis.com/drive/v2/files/'+path,
    headers: headers_google
  },function(err,response,body){
    if(!err && (response.statusCode == 204)){
      callback(body);
    } else {
      tools.internalError(res  ,'Google Drive','delete',body);
    }
  });
} //endDelete


GDriveController.prototype.edit = function(path, newName, req, res, callback) {

  this.token = req.cookies.googleDriveCookie;
  let headers_google = {
    "Content-Type": "application/json",
    "Authorization":"Bearer "+this.token.access_token
  };
  let body_google = {
    "title": newName
  }
  request.patch({
    url:'https://www.googleapis.com/drive/v2/files/'+path,
    headers: headers_google,
    json: body_google
  },function(err,response,body){
    if(!err && (response.statusCode == 200)){
      callback(body);
    } else {
      tools.internalError(res ,'Google Drive','edit',body );
    }
  });
} //endEdit


GDriveController.prototype.move = function(google_id, old_folder_id, new_folder_id, req, res, callback) {

  this.token = req.cookies.googleDriveCookie;
  let headers_google = {
    "Content-Type": "application/json",
    "Authorization":"Bearer "+this.token.access_token
  };

  let body_google = JSON.stringify({id: new_folder_id});
  request.post({
    url:'https://www.googleapis.com/drive/v2/files/'+google_id+'/parents',
    headers: headers_google,
    body: body_google
  },function(err,response,body){
    if(!err && (response.statusCode == 200)){
      request.delete({
        url:'https://www.googleapis.com/drive/v2/files/'+google_id+'/parents/'+old_folder_id,
        headers: headers_google
      },function(err,response,body){
        if(!err && (response.statusCode == 204)){
          body.google_id = body.id;
          callback(body);
        }else{
          tools.internalError(res ,'Google Drive','move (delete)',body );
        }
      });
    }else{
      tools.internalError(res ,'Google Drive','move',body );
    }
  });
} //endMove


GDriveController.prototype.download = function(path, req,res, callback) {
  this.token = req.cookies.googleDriveCookie;
  let headers_google = {
    "Content-Type": "application/json",
    "Authorization":"Bearer "+this.token.access_token
  };
  request.get({
    url:'https://www.googleapis.com/drive/v2/files/'+path,
    headers: headers_google
  },function(err,response,body){
    if(!err && (response.statusCode == 200)){
      body = JSON.parse(body);
      let resp = {
        'link' : body.webContentLink
      }
      callback(resp);
    } else if(response.statusCode == 401){

    } else {
      tools.internalError(res ,'Google Drive','download',body );
    }
  });
} //endDownload

GDriveController.prototype.share = function(google_id, mail, req,res, callback) {
  this.token = req.cookies.googleDriveCookie;
  let headers_google = {
    "Content-Type": "application/json",
    "Authorization":"Bearer "+this.token.access_token
  };

  let body_google = {
    "role": "writer",
    "type": "user",
    "emailAddress": mail
  };

  request.post({
    url:'https://www.googleapis.com/drive/v3/files/'+google_id+'/permissions',
    headers: headers_google,
    body: JSON.stringify(body_google)
  },function(err,response,body){
    if(!err && (response.statusCode == 200)){
      body = JSON.parse(body);
      callback(body);
    } else {
      tools.internalError(res ,'Google Drive','share',body );
    }
  });
} //endShare

GDriveController.prototype.upload = function(folder_id, name, mimetype_file, fileData, req, res, callback) {

  this.token = req.cookies.googleDriveCookie;
  let bound = "foo_bar_baz"
  let headers_google = {
    "Content-Type": "application/json",
    //"Content-Type": "multipart/related; boundary="+bound,
    "Authorization":"Bearer "+this.token.access_token,
  };

  let metadata = {"title": name,
  "parent": [{ "id" : folder_id }]}

  var body_google = "--"+bound+"\n"+
  "\n"+
  "Content-Type: application/json; charset=UTF-8\n"+
  "\n"+
  JSON.stringify(metadata)+"\n"+
  "\n"+
  // "--"+bound+"\n"+
  // "\n"+
  // "Content-Type: "+ mimetype_file+"\n"+
  // "\n"+
  // fileData+
  // "\n"+
  "--"+bound+"--";

  console.log(body_google);

  request.post({
    url:'https://www.googleapis.com/drive/v2/files',
    headers: headers_google,
    body: JSON.stringify(metadata)
  },function(err,response,body){
    if(!err && (response.statusCode == 200)){
      body = JSON.parse(body);
      body.name = body.title;
      body.google_id = body.id;
      callback(body);
    } else {
      tools.internalError(res, body, 'Google Drive','upload');
    }
  });
} //endUpload

GDriveController.prototype.space = function(req,res, callback) {
  this.token = req.cookies.googleDriveCookie;

  let headers = {
    "Content-Type": "application/json",
    "Authorization":"Bearer "+this.token.access_token
  };

  request.get({
    url:'https://www.googleapis.com/drive/v2/about',
    headers: headers,
  },function(err,response,body){
    if(!err && (response.statusCode == 200)){
      body = JSON.parse(body);
      callback(body);
    } else {
      tools.internalError(res,'Google Drive','space',body);
    }

  });
}

module.exports = GDriveController;
